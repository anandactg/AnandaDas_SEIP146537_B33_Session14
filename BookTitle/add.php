<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- bootstrap codes that
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
    <link rel="stylesheet" href="../asset/BootStrap/css/bootstrap.min.css">
    <script src="../asset/BootStrap/js/jquery.min.js"></script>
    <script src="../asset/BootStrap/js/bootstrap.min.js"></script>

</head>
<body>
<header>
    <div class="container-fluid">
        <div class="form-group">
            <h3 align="center" style="color: #2aabd2"><u>Atomic Project</u> </h3>
            </div>




</header>

<div class="container">
    <h2 align="center" style="color: #3e8f3e">All Book Information</h2>
    <form>
        <div class="form-group">
            <label for="email">Book Title : </label>
            <input type="text" class="form-control" id="text" placeholder="Enter book name">
        </div>
        <div class="form-group">
            <label for="pwd">Author Name : </label>
            <input type="text" class="form-control" id="text" placeholder="Enter author name">
        </div>

        <button type="submit" class="btn btn-danger">Save</button>
        <button type="submit" class="btn btn-info">Add & List</button>
        <button type="submit" class="btn btn-group">Reset</button>
        </div>

    </form>

<footer>
    <div class="container-fluid">
        <div class="form-group">
            <h3 align="center" style="color: #985f0d; font-size: medium">Project done by Ananda Das, SEIP146537, Batch-33, PHP F2</h3>
        </div>

    <footer>
</div>

</body>
</html>

